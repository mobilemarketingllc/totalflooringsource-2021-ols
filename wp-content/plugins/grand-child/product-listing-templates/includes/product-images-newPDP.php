<?php
        $collection = $meta_values['collection'][0];  
        
        $image = swatch_image_product(get_the_ID(),'600','400');
      //  $image_thumb = swatch_image_product_thumbnail(get_the_ID(),'222');
?>
<div <?php if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall'  || $collection == 'Floorte Magnificent') { ?>class="colorwall-exlusive-batch roomImageSlider  "<?php }else { ?> class="roomImageSlider " <?php } ?>>
<div class="slides">
    <?php  
        if (!empty($image)){
    ?>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <img src="<?php echo $image; ?>" class="slider-img" alt="<?php the_title_attribute(); ?>" />
        </div> 
    <?php } ?>
    
    <?php
        // check if the repeater field has rows of data
        //var_dump($meta_values );die(); 

        
        if(array_key_exists("gallery_room_images",$meta_values) && $meta_values['gallery_room_images'][0]!=''){
            $gal_count = 1;
            $gallery_images = $meta_values['gallery_room_images'][0];
            $gallery_img = explode("|",$gallery_images);
            // loop through the rows of data
            foreach($gallery_img as  $key=>$value) {

                $room_image =  high_gallery_images_in_loop($value);
                $room_image_thumb =  thumb_gallery_images_in_loop($value);      
        ?>    
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <img src="<?php echo $room_image; ?>" class="slider-img" alt="<?php the_title_attribute(); ?>" />    
                </div>    
            <?php
            $gal_count++;
            }
        }
        ?>
</div>
</div>
