<?php 
global $post;
$flooringtype = $post->post_type;


?>
<div class="product-attributes">
    <table class="table">
        <thead>
        <tbody>
        <?php if(array_key_exists("parent_collection",$meta_values) && $meta_values['parent_collection'][0]!=''){ ?>
            <tr>
                
                <td><strong>Main Collection</strong><?php echo $meta_values['parent_collection'][0]; ?></td>
            </tr>
        <?php } ?>

        <?php if(array_key_exists("collection",$meta_values) && $meta_values['collection'][0]!=''){ ?>
            <tr>
                <td><strong>Collection</strong><?php echo $meta_values['collection'][0]; ?></td>
            </tr>
        <?php } ?>

        <?php if(array_key_exists("color",$meta_values) && $meta_values['color'][0]!=''){  ?>
            <tr>
                
                <td><strong>Color</strong><?php echo $meta_values['color'][0]; ?></td>
            </tr>
        <?php } ?>

        <?php if(array_key_exists("construction",$meta_values) && $meta_values['construction'][0]!=''){  ?>
            <tr>
                <td><strong>Construction</strong><?php echo $meta_values['construction'][0]; ?></td>
            </tr>
        <?php } ?>
        <?php if(array_key_exists("color_variation",$meta_values) && $meta_values['color_variation'][0]!=''){  ?>
         <tr>
            <td><strong>Color Variation</strong><?php echo $meta_values['color_variation'][0]; ?></td>
        </tr>
        <?php } ?>
        
        <?php if(array_key_exists("shade",$meta_values)  && $collection != 'COREtec Colorwall' && $meta_values['shade'][0]!='') { ?>
                        <tr>
                            <td><strong>Shade</strong><?php echo $meta_values['shade'][0]; ?></td>
                        </tr>
       <?php } ?>
		<?php if(array_key_exists("shape",$meta_values) && $meta_values['shape'][0]!=''){ ?>
            <tr>
                <td><strong>Shape</strong><?php echo $meta_values['shape'][0]; ?></td>
            </tr>
        <?php } ?>

        <?php if(array_key_exists("core",$meta_values) && $meta_values['core'][0]!=''){?>
                        <tr>
                            <td><strong>Core</strong><?php echo $meta_values['core'][0] ;?></td>
                        </tr>
         <?php } ?>

        <?php if(array_key_exists("species",$meta_values) && $meta_values['species'][0]!=''){?>
        <tr>
            <td><strong>Species</strong><?php echo $meta_values['species'][0]; ?></td>
        </tr>
        <?php } ?>
        <?php if(array_key_exists("fiber_type",$meta_values) && $meta_values['fiber_type'][0]!=''){ ?>
            <tr>
                <td><strong>Fiber Type</strong><?php echo $meta_values['fiber_type'][0]; ?></td>
            </tr>
        <?php } ?>
        <?php if(array_key_exists("surface_type",$meta_values) && $meta_values['surface_type'][0]!=''){?>
            <tr>
                <td><strong>Surface Type</strong><?php echo $meta_values['surface_type'][0]; ?></td>
            </tr>
        <?php } ?>
			<?php if(array_key_exists("surface_texture_facet",$meta_values) && $meta_values['surface_texture_facet'][0]!=''){  ?>
            <tr>
                <td><strong>Surface Texture</strong><?php echo $meta_values['surface_texture_facet'][0]; ?></td>
            </tr>
        <?php } ?>
        <?php if(array_key_exists("style",$meta_values) && $meta_values['style'][0]!='' ){  ?>
            <tr>
                <td><strong>Style</strong><?php echo $meta_values['style'][0]; ?></td>
            </tr>
        <?php } ?>
        <?php if(array_key_exists("edge",$meta_values) && $meta_values['edge'][0]!=''){  ?>
            <tr>
                <td><strong>Edge</strong><?php echo $meta_values['edge'][0]; ?></td>
            </tr>
        <?php } ?>
        <?php if(array_key_exists("application",$meta_values) && $meta_values['application'][0]!=''){ ?>
            <tr>
                <td><strong>Application</strong><?php echo $meta_values['application'][0]; ?></td>
            </tr>
        <?php } ?>
        <?php if($flooringtype=="hardwood_catalog" || $flooringtype=="luxury_vinyl_tile" || $flooringtype=="laminate_catalog") {} else { ?>
            <?php if(array_key_exists("size",$meta_values) && $meta_values['size'][0]!=''){ ?>
                <tr>
                    <td><strong>Size</strong><?php echo $meta_values['size'][0]; ?></td>
                </tr>
            <?php } ?>
        <?php } ?>
        <?php if(array_key_exists("width",$meta_values) && $meta_values['width'][0]!=''){ ?>
            <tr>
                <td><strong>Width</strong><?php echo $meta_values['width'][0]; ?></td>
            </tr>
        <?php } ?>

         <?php if($flooringtype != "carpeting"){ ?>   

        <?php if(array_key_exists("length",$meta_values) && $meta_values['length'][0]!=''){ ?>
            <tr>
                <td><strong>Length</strong><?php echo ucwords(strtolower($meta_values['length'][0])); ?></td>
            </tr>
        <?php } ?>
        <?php if(array_key_exists("thickness",$meta_values) && $meta_values['thickness'][0]!=''){  ?>
            <tr>
                <td><strong>Thickness</strong><?php echo $meta_values['thickness'][0]; ?></td>
            </tr>
        <?php } ?>



        <?php } ?>
        
        <?php if(array_key_exists("plank_dimensions",$meta_values) && $meta_values['plank_dimensions'][0]!=''){ ?>
                        <tr>
                            <td><strong>Plank Dimensions</strong><?php echo $meta_values['plank_dimensions'][0]; ?></td>
                        </tr>
         <?php } ?>

         
         <?php if(array_key_exists("flooring_type",$meta_values) && $meta_values['flooring_type'][0]!=''){ ?>
                        <tr>
                            <td><strong>Flooring Type</strong><?php echo $meta_values['flooring_type'][0]; ?></td>
                        </tr>
                        <?php } ?>
                        
         <?php if(array_key_exists("edge_profile",$meta_values) && $meta_values['edge_profile'][0]!=''){ ?>
                        <tr>
                            <td><strong>Edge Profile</strong><?php echo $meta_values['edge_profile'][0]; ?></td>
                        </tr>
          <?php } ?>        
			
			<?php if(array_key_exists("location",$meta_values) && $meta_values['location'][0]!=''){ ?>
            <tr>
                <td><strong>Location</strong><?php echo $meta_values['location'][0]; ?></td>
            </tr>
        <?php } ?>
			<?php if(array_key_exists("backing_facet",$meta_values) && $meta_values['backing_facet'][0]!=''){?>
            <tr>
                <td><strong>Backing</strong><?php echo $meta_values['backing_facet'][0]; ?></td>
            </tr>
        <?php } ?>
			<?php if(array_key_exists("look",$meta_values) && $meta_values['look'][0]!=''){ ?>
            <tr>
                <td><strong>Look</strong><?php echo $meta_values['look'][0]; ?></td>
            </tr>
        <?php } ?>         

        <?php if(array_key_exists("installation_method",$meta_values)  && $meta_values['installation_method'][0]!=''){ ?>
            <tr>
                <td><strong>Installation Method</strong><?php echo $meta_values['installation_method'][0]; ?></td>
            </tr>
        <?php } ?>      

         <?php  if(array_key_exists("installation_level",$meta_values) && $meta_values['installation_level'][0]!=''){ ?>
                        <tr>
                            <td><strong>Installation Level</strong><?php echo $meta_values['installation_level'][0]; ?></td>
                        </tr>
        <?php } ?>
			
        </tbody>
    </table>
    </div>