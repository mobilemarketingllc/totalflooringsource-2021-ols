<div class="product-detail-layout-3">
<style>
/* Style the list */
ul.tab {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    display: flex;
    align-items: center;
    justify-content: space-around;
}

/* Float the list items side by side */
ul.tab li {float: left;}

/* Style the links inside the list items */
ul.tab li a {
    display: inline-block;
    color: black;
    text-align: center;
    padding: 8px 16px;
    text-decoration: none;
    transition: 0.3s;
    font-size: 17px;
}
.tablinks.active{ 
    border-bottom: 3px solid #db8d11;!important;
}

/* Style the tab content */
.tabcontent {
    display: none;
    padding: 6px 12px;
    border-top: none;
}
.product-attributes tr td {
    text-align: center;
}
.product-attributes tr td strong {
    margin-right: 5px;
}
</style>

<?php
global $post;
$flooringtype = $post->post_type; 
$meta_values = get_post_meta( get_the_ID() );
$brand = $meta_values['brand'][0] ;
$sku = $meta_values['sku'][0];
$manufacturer = $meta_values['manufacturer'][0];
$image = swatch_image_product(get_the_ID(),'600','400');
$getcouponbtn = get_option('getcouponbtn');
$getcouponreplace = get_option('getcouponreplace');
$getcouponreplacetext = get_option('getcouponreplacetext');
$getcouponreplaceurl = get_option('getcouponreplaceurl');
$pdp_get_finance = get_option('pdp_get_finance');
$getfinancereplace = get_option('getfinancereplace');
$getfinancereplaceurl = get_option('getfinancereplaceurl');
$getfinancetext = get_option('getfinancetext');

if(get_option('salesbrand')!=''){
	$slide_brands = rtrim(get_option('salesbrand'), ",");
	$brandonsale = array_filter(explode(",",$slide_brands));
	$brandonsale = array_map('trim', $brandonsale);
}
$image = swatch_image_product(get_the_ID(),'600','400');
?>
	<article <?php post_class( 'fl-post' ); ?> id="fl-post-<?php the_ID(); ?>">
		<div class="fl-post-content clearfix grey-back newpdp" itemprop="text">
		<div class="container">	
			<div class="row">
				<div class="col-md-6 col-sm-12 product-swatch">   
					<?php 
						$dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-images-newPDP.php';
						include( $dir );
					?>
				</div>
				
				<div class="col-md-6 col-md-12 product-box ">
					<!-- <?php
						$dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-brand-logos.php';
						include_once( $dir );
					?> -->
						<div class="clearfix"></div>
						<!-- <?php if(array_key_exists("parent_collection",$meta_values)){?>
						<h4><?php echo $meta_values['parent_collection'][0]; ?></h4>
						<?php } ?> -->
					<?php if(array_key_exists("collection",$meta_values)){?>
					
					<h1 class="fl-post-title text-center" itemprop="name"><?php echo $meta_values['collection'][0]; ?></h1>
					<?php } ?>
					<div style="text-align: center!important;"><div class="customSeparator"></div></div>
					<div class="BrandingHolder text-center">
						<h6><?php echo $meta_values['collection'][0];?> COLLECTION</h6>
						<em>By <a href="#"><?php echo $meta_values['brand_facet'][0];?></a></em>
					</div>	
					
					<?php /*?><h5>Style No. <?php the_field('style'); ?>, Color No. <?php the_field('color_no'); ?></h5><?php*/ ?>
					<div class="product-colors">
					<?php
								$collection = $meta_values['collection'][0];    
								$satur = array('Masland','Dixie Home');  
								
							if($collection != NULL ){
								
								if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall') {

									$familycolor = $meta_values['style'][0];
									$key = 'style';

								}else{

									$familycolor = $meta_values['collection'][0];    
									$key = 'collection';
									
								}	
							}else{	
								
								if (in_array($meta_values['brand'][0], $satur)){
									$familycolor = $meta_values['design'][0];    
									$key = 'design';
								}

							}

								$args = array(
									'post_type'      => $flooringtype,
									'posts_per_page' => -1,
									'post_status'    => 'publish',
									'meta_query'     => array(
										array(
											'key'     => $key,
											'value'   => $familycolor,
											'compare' => '='
										),
										array(
											'key' => 'swatch_image_link',
											'value' => '',
											'compare' => '!='
											)
									)
								);										

								$the_query = new WP_Query( $args );
								
							?>
						<ul>
							<li class="color-count " style="font-size:14px;"><?php echo $the_query->found_posts; ?> Colors Available</li>
						</ul>
					</div>
					<div class="clearfix"></div>
					<div class="swatcImageHolder">
						<img src="<?php echo $image; ?>" alt="<?php the_title_attribute(); ?>" class="swatch-images" />	
					</div>
					<div class="clearfix"></div>	
					<div class="color-slider-new">
						<?php
							$dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-color-slider-newPdp.php';
							include( $dir );
						?>		
					</div>		
					<div class="clearfix"></div>			
					<div class="button-wrapper">
						<a href="/contact/"  class="button contact-btn">Contact Us</a>	
						<a href="/measurement-tool/"  class="button appointment-btn ">Measurement Tool</a>	
						<!-- <a href="#"  class="button sample-order-btn ">+ ORDER A SAMPLE</a>			 -->
						<!-- <?php  if(( get_option('getcouponbtn') == 1 && get_option('salesbrand')=='') || (get_option('getcouponbtn') == 1 && in_array(sanitize_title($brand),$brandonsale))){  ?>
												<a href="<?php if($getcouponreplace ==1){ echo $getcouponreplaceurl ;}else{ echo '/flooring-coupon/'; } ?>" target="_self" class="button alt getcoupon-btn" role="button" <?php //get_coupon_button_visibility($sale_arr,$brand_arr); ?> >
												<?php if( $getcouponreplace ==1){ echo $getcouponreplacetext ;}else{ echo 'GET COUPON'; }?>
												</a>
						<?php } ?> -->
						
						
						<!-- <?php if($pdp_get_finance != 1 || $pdp_get_finance  == '' ){?>						
							<a href="<?php if($getfinancereplace ==1){ echo  $getfinancereplaceurl ;}else{ echo '/flooring-financing/'; } ?>" class="finance-btn button"><?php if($getfinancereplace =='1'){ echo $getfinancetext ;}else{ echo 'Get Financing'; } ?></a>	
						<?php } ?>  -->

						<?php roomvo_script_integration($manufacturer,$sku,get_the_ID());?>

					<!-- 	 <span class="divider">&nbsp;|&nbsp;</span> <a href="/schedule-appointment/">Schedule a Measurement</a> -->
					</div>
				</div>
					
			</div>
			</div>				
		</div>

		
		<div class="product-content-tab">
			<div class="container">							
				<ul class="tab">
					<li><a href="javascript:void(0)" id="defaultOpen" class="tablinks active" onclick="openCity(event, 'product-spec')">PRODUCT SPECS</a></li>
					<li><a href="javascript:void(0)" class="tablinks" onclick="openCity(event, 'product-desc')">PRODUCT DESCRIPTION</a></li>
					<!-- <li><a href="#" class="tablinks" onclick="openCity(event, 'product-download')">DOWNLOADS</a></li>
					<li><a href="#" class="tablinks" onclick="openCity(event, 'product-info')">RELATED INFORMATION</a></li>						 -->
				</ul>
				<div id ="product-spec" class="tabcontent">
					<div id="product-attributes-wrap">
						<?php //get_template_part('includes/product-attributes');
						$dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-attributes-newPDP.php';
						include( $dir );
						?>
					</div>					
				</div>		
				<div id ="product-desc" class="tabcontent" style="display: block;">
					<div>				
						<h2 style="text-align: center;">DYNASTY</h2>
						<h4 style="text-align: center;">THE VIN COLLECTION</h4>
						<p style="text-align: center;">Warm up rooms and reduce energy bills with the natural insulation and comfort of carpet. 
							With many styles to choose from and the best stain-fighting technology in the industry, 
							Shaw carpet brings color, texture and value to your floors. Create distinctively stylish looks when you play with texture, 
							loop, pattern and twist options.
						Warm up rooms and reduce energy bills with the natural insulation and comfort of carpet.
						 With many styles to choose from and the best stain-fighting technology in the industry, 
						 Shaw carpet brings color, texture and value to your floors. 
						 Create distinctively stylish looks when you play with texture, loop, pattern and twist options.</p>	
					</div>	 			
				</div>
				<!-- <div id ="product-download" class="tabcontent"></div>		
				<div id ="product-info" class="tabcontent"></div> -->
			</div>
		</div>

		<div class="Above-footer-section">
					
		<?php echo do_shortcode('[fl_builder_insert_layout slug="pdp-above-footer-template"]');	?>	
		</div>
		
	</article>
</div>
<?php
			$title = get_the_title();

			$jsonld = array('@context'=>'https://schema.org/','@type'=>'Product','name'=> $title,'image'=>$image,'description'=>$title,'sku'=>$sku,'mpn'=>$sku,'brand'=>array('@type'=>'thing','name'=>$brand), 
			'offers'=>array('@type'=>'offer','priceCurrency'=>'USD','price'=>'00','priceValidUntil'=>''));
			?>
			<?php echo '<script type="application/ld+json">'.json_encode($jsonld).'</script>';	?>
			<script>
// function openCity(evt, cityName) {
//     var i, tabcontent, tablinks;
//     tabcontent = document.getElementsByClassName("tabcontent");
//     for (i = 0; i < tabcontent.length; i++) {
//         tabcontent[i].style.display = "none";
//     }
//     tablinks = document.getElementsByClassName("tablinks");
//     for (i = 0; i < tabcontent.length; i++) {
//         tablinks[i].className = tablinks[i].className.replace(" active", "");
//     }
//     document.getElementById(cityName).style.display = "block";
//     evt.currentTarget.className += " active";
// }

function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>