    <div class="product-plp-grid product-grid swatch" itemscope itemtype="http://schema.org/ItemList">

    <div class="row product-row newPLP">
    
 <div class="overlayInquery" style="display: none!important;">
 	<div class='measurepopup'>
	    <div class='row'>
		    <div class='col-lg-7'>
		    	
		    [fl_builder_insert_layout slug="product-inquery-row"]
		    </div>
		    <div class='col-lg-5 sharingBox'>

		            <a href='javascript:void(0);' onclick="closepopupoverlay()" class="closePopup">
		                <i class='fa fa-times' aria-hidden='true'></i>
		            </a>	
                <div class="productInfo" style="padding-top: 60px;">
                        <img src="/wp-content/uploads/2020/10/Bitmap-9.png" name="Product Name" alt="Product Image" /> 
                        <h3> Product name </h3>
                        <p> Brand name </p>
                </div>
		    </div>
	     </div> 
	</div>
</div>	
    <?php 
    global $wpdb;
    $show_financing = get_option('sh_get_finance');
    $col_class = 'col-lg-3 col-md-4 col-sm-6';
    $salebrand = get_option('salesbrand');
    if($salebrand !=''){
        $slide_brands = rtrim($salebrand, ",");
        $brandonsale = array_filter(explode(",",$slide_brands));
        $brandonsale = array_map('trim', $brandonsale);
    }
    $K = 1;

$getcouponbtn = get_option('getcouponbtn');
$getcouponreplace = get_option('getcouponreplace');
$getcouponreplacetext = get_option('getcouponreplacetext');
$getcouponreplaceurl = get_option('getcouponreplaceurl');
$pdp_get_finance = get_option('pdp_get_finance');
$getfinancereplace = get_option('getfinancereplace');
$getfinancereplaceurl = get_option('getfinancereplaceurl');
$getfinancetext = get_option('getfinancetext');

while ( $prod_list->have_posts() ): $prod_list->the_post(); 
      //collection field
      $meta_values = get_post_meta( get_the_ID() );  
      $collection = $meta_values['collection'][0];
      $brand =  $meta_values['brand'][0];      
      $flooringtype = get_post_type();
      $product_url = get_the_permalink();
?>
    <div class="<?php echo $col_class; ?>">    
    <div class="fl-post-grid-post" itemprop="itemListElement" itemscope  itemtype="http://schema.org/ListItem">

    <meta itemprop="position" content="<?php echo $K; echo $prod_list->ID;?>" />
        <?php // FLPostGridModule::schema_meta(); ?>
        <?php if($meta_values['swatch_image_link'][0]) { ?>
            <div class="fl-post-grid-image">
            <!-- <div style="display:none;"><?php echo is_user_logged_in();?></div> -->
            <?php
            
            $user = wp_get_current_user();
if($user->ID != 0 or $user->ID != null){
            //if (is_user_logged_in()) { 

            echo '<style>.favProdPLP{display:block !important;} </style>';
            }

                $fav_sql = 'SELECT * FROM wp_favorite_posts WHERE user_id = '.$user->ID.' and product_id = '. get_the_ID().'';

                $check_fav = $wpdb->get_results($fav_sql);         

                if(!empty($check_fav)) {
                    $link_action = 'rem_fav';
                    $icon_class = 'fa fa-heart';
                }else{
                    $link_action = 'add_Fav';
                    $icon_class = 'fa fa-heart-o';
                }
                ?> 
                <div class="favProdPLP" style="display:none;">
                <a  class="favProdPdp <?php echo $link_action; ?>" data-user="<?php echo get_current_user_id(); ?>" data-id="<?php echo get_the_ID(); ?>"><i class="<?php echo $icon_class; ?>" aria-hidden="true"></i></a> 
                </div>
                

                  <?php 
                  
                  if( get_option('plpproductimg') !='' && get_option('plpproductimg') == '1' ){
                    $gallery_images = $meta_values['gallery_room_images'][0];
                    $gallery_img = explode("|",$gallery_images);
                    $image =  $gallery_img[0] ? thumb_gallery_images_in_plp_loop($gallery_img[0]) : swatch_image_product_thumbnail(get_the_ID(),'222','222');
                     
                  }else{
                    $image = swatch_image_product_thumbnail(get_the_ID(),'222','222');
                    
                  }	
					?>
            <img class="list-pro-image" src="<?php  echo $image; ?>" alt="<?php the_title_attribute(); ?>" />
            
            <?php
            // exclusive icon condition
            if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall' ||  $collection == 'Floorte Magnificent') {    ?>
			<span class="exlusive-badge"><img src="<?php echo plugins_url( '/product-listing-templates/images/exclusive-icon.png', dirname(__FILE__) );?>" alt="<?php the_title(); ?>" /></span>
			<?php } ?>
                  
                </a>
            </div>
        <?php } else { ?>
            <div class="fl-post-grid-image">
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <?php //the_post_thumbnail($settings->image_size); ?>
                    <img src="http://placehold.it/300x300?text=No+Image" alt="<?php the_title_attribute(); ?>" />
                </a>
            </div>

        <?php } ?>
        <div class="fl-post-grid-text product-grid btn-grey">
           <h4><?php if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall') { ?><span>  <span><?php the_field('style'); ?> </span> <?php the_field('collection'); ?></span> <?php } else{ ?> <span><?php the_field('brand'); ?> </span> <span><?php the_field('collection'); ?></span>  <?php } ?> </h4> 
            
            <?php
                $collection = $meta_values['collection'][0]; 
                                                             
                
                if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall') {

                    $familycolor = $meta_values['style'][0];
                    $key = 'style';

                }else{

                    $familycolor = $meta_values['collection'][0];    
                    $key = 'collection';
                }	
                
                $args = array(
                    'post_type'      => $flooringtype,
                    'post_status'    => 'publish',
                    'posts_per_page' => -1,
                    'meta_query'     => array(
                                            array(
                                                'key'     => $key,
                                                'value'   => $familycolor,
                                                'compare' => '='
                                            ),
                                            array(
                                                'key'     => 'swatch_image_link',
                                                'value'   => '',
                                                'compare' => '!='
                                            )
                                        )
                            );										
                            remove_filter('posts_groupby', 'query_group_by_filter'); 

                            $familycolor = addslashes($familycolor);
                            
                            // $the_query = new WP_Query( $args );

                            $table_posts = $wpdb->prefix.'posts';
                            $table_meta = $wpdb->prefix.'postmeta';	
                
                            $coll_sql = "select ID,post_title,guid,$table_meta.meta_value,(SELECT count(meta_id) as cnt 
                            FROM $table_meta
                            WHERE meta_key = '$key' AND meta_value = '$familycolor') as cntt from $table_posts inner join $table_meta on $table_posts.ID=$table_meta.post_id and 
                            meta_key='swatch_image_link' where ID in(SELECT post_id FROM $table_meta WHERE meta_key = '$key' AND meta_value = '$familycolor'
                            ) LIMIT 6";

                         //  $data_collection = $wpdb->get_results($coll_sql);
                           
                ?>
            <p class="productInquery" data-img="<?php  echo $image; ?>" data-page="<?php the_permalink(); ?>" data-collection="<?php the_field('collection'); ?>" data-brand="<?php if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall') {  the_field('style'); } else{ the_field('brand');} ?>">PRODUCT INQUIRY</p>
            <a class="fl-button plp_box_btn" href="<?php the_permalink(); ?>">VIEW PRODUCT</a><br>
            <!-- <a class="fl-button" href="#">SEE IN ROOM</a> <br> -->
           <?php
           //var_dump(in_array(sanitize_title($brand),$brandonsale),sanitize_title($brand),$brandonsale);
           if(( $getcouponbtn == 1 && $salebrand =='') || ($getcouponbtn == 1 && in_array(sanitize_title($brand),$brandonsale))){?>
                <a href="<?php if($getcouponreplace == 1){ echo $getcouponreplaceurl;}else{ echo '/flooring-coupon/'; } ?>" target="_self" class="link " role="button" <?php //get_coupon_button_visibility($sale_arr,$brand_arr); ?> >
                <span class="fl-button-text"><?php if($getcouponreplace == 1){ echo $getcouponreplacetext ; }else{ echo 'GET COUPON'; }?></span>
            </a>
            </a><br />
            <?php } ?>

           <?php if($show_financing == 1){?>
            <a href="<?php if($getfinancereplace == 1){ echo $getfinancereplaceurl;}else{ echo '/flooring-financing/'; } ?>" target="_self" class="link" role="button" >
                <span class="fl-button-text"><?php if( $getfinancereplace == '1'){ echo $getfinancetext ;}else{ echo 'Get Financing'; } ?></span>
            </a>
            <br />
           <?php } ?>
           
           
          
           
        </div>
    </div>
    </div>
<?php  $K++; endwhile; wp_reset_postdata();?>
</div>
</div>
