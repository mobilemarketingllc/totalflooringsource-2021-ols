<?php
/**
 * Astra Child Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Astra Child
 * @since 1.0.0
 */

/**
 * Define Constants
 */
define( 'CHILD_THEME_ASTRA_CHILD_VERSION', '1.0.0' );

/**
 * Enqueue styles
 */
function child_enqueue_styles() {

	wp_enqueue_style( 'astra-child-theme-css', get_stylesheet_directory_uri() . '/style.css', array('astra-theme-css'), CHILD_THEME_ASTRA_CHILD_VERSION, 'all' );

}

add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 15 );


add_action( 'wp_enqueue_scripts', function(){

	wp_enqueue_style( 'style-name', get_stylesheet_directory_uri()."/base.min.css", array(), "", false);
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);
    wp_enqueue_script("PDF_script","https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.9.2/html2pdf.bundle.min.js","","",1);
    wp_enqueue_script("shareBox-script", get_stylesheet_directory_uri()."/resources/sharebox/needsharebutton.js","","",1);
    wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);
});

/**
 * Dequeue the jQuery UI styles.
 *
 * Hooked to the wp_print_styles action, with a late priority (100),
 * so that it is after the style was enqueued.
 */


function remove_pagelist_css() {
    wp_dequeue_style( 'page-list-style' );
 }
 add_action( 'wp_print_styles', 'remove_pagelist_css', 100 );


 //Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.@$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );


//Yoast SEO Breadcrumb addded
function bbtheme_yoast_breadcrumb() {
    if ( function_exists('yoast_breadcrumb') && ! is_front_page() ) {
        yoast_breadcrumb('<div id="breadcrumbs"><div class="container">','</div></div>');
    }
}
add_action( 'astra_header_after', 'bbtheme_yoast_breadcrumb' );


function new_year_number()
{
return $new_year = date('Y');
}
add_shortcode('year_code', 'new_year_number');


function replace_core_jquery_version() {
    wp_deregister_script( 'jquery' );
    // Change the URL if you want to load a local copy of jQuery from your own server.
    wp_register_script( 'jquery', "https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js", array(), '1.12.4' );
}
add_action( 'wp_enqueue_scripts', 'replace_core_jquery_version' );


add_action( 'wp_ajax_nopriv_add_fav_product', 'add_fav_product' );
add_action( 'wp_ajax_add_fav_product', 'add_fav_product' );

function add_fav_product() {
    $is_fav = $_POST['is_fav'];
    $post_id = $_POST['post_id'];

    $result = update_post_meta( $post_id, 'is_fav', $is_fav);
if($result == false){
    $result1 = add_post_meta( $post_id, 'is_fav', $is_fav);
}
}

add_action( 'wp_ajax_nopriv_base64_to_jpeg_convert', 'base64_to_jpeg_convert' );
add_action( 'wp_ajax_base64_to_jpeg_convert', 'base64_to_jpeg_convert' );

function base64_to_jpeg_convert() {    
    global $wpdb;

    $upload_dir = wp_get_upload_dir();
    $output_file= $upload_dir['basedir']. '/measure/'.uniqid().'.png';

    $img = $_POST['imagedata']; 
    $img = str_replace('data:image/png;base64,', '', $img);
    $img = str_replace(' ', '+', $img);
    $data = base64_decode($img);
    file_put_contents($output_file, $data);
    
    if ( is_user_logged_in() ) {
        $current_user = wp_get_current_user();
        write_log( 'Personal Message For '. $current_user->ID ) ;
     
        $data=array(
            'userid' => $current_user->ID, 
            'image_name' => $_POST['imagename'],
            'image_path' => $output_file 
        );    
    
         $wpdb->insert( 'wp_measure_images', $data);

         $measureimages = $wpdb->get_results("SELECT * FROM wp_measure_images WHERE userid = $current_user->ID ");

        write_log($measureimages);

        
        $content = '<div id="mesureMentprintMe" class="product-plp-grid product-grid swatch "><div class="row product-row" id="aniimated-thumbnials">';

        $content .= '<h3 style="text-align: center; font-size: 33px;"><span>SAVED MEASUREMENTS</span></h3>';

        foreach($measureimages as $img){

           

            $img_path = str_replace('/var/www/html', '', $img->image_path);

            $image_url = home_url().''.$img_path;

            $content .= '<div class="col-lg-4 col-md-4 col-sm-6 "><div class="fl-post-grid-post">';
            $content .= '<div class="fl-post-grid-image prod_like_wrap mesureMentWrap">';

            $content .= '<a href="javascript:void(0);" title="#" class="wplightbox"><img class="measure_img" src="'.home_url().''.$img_path.'" /></a>';

            $content .= '<div class="favButtons button-wrapper">
                <a class="button fl-button view" href="javascript:void(0);" title="#"  data-img="'.home_url().''.$img_path.'" onclick="openPopUp(this)" data-title="'.$img->image_name.'">VIEW</a>';
                $content .= '<a class="button fl-button deletemeasure" href="javascript:void(0)" data-id="'.$img->id.'"><i class="fa fa-trash" aria-hidden="true"></i></a>'; 
                $content .= '</div> </div>';
                $content .= '<h3><span>'.$img->image_name.'</span></h3>';

                $content .= "</div></div>";

        }

        $content .= "</div></div>";

        echo $content;


        wp_die();
    }

}

function measurement_tool_images($arg){    
    global $wpdb;

    if ( is_user_logged_in() ) {
        $current_user = wp_get_current_user();
        write_log( 'Personal Message For '. $current_user->ID ) ;

        $measureimages = $wpdb->get_results("SELECT * FROM wp_measure_images WHERE userid = $current_user->ID ");

        write_log($measureimages);

        
        $content = '<div id="mesureMentprintMe" class="product-plp-grid product-grid swatch "><div class="row product-row" id="aniimated-thumbnials">';
        $content .= '<h3 style="text-align: center; font-size: 33px;"><span>SAVED MEASUREMENTS</span></h3>';

        foreach($measureimages as $img){

           

            $img_path = str_replace('/var/www/html', '', $img->image_path);

            $image_url = home_url().''.$img_path;

            $content .= '<div class="col-lg-4 col-md-4 col-sm-6 "><div class="fl-post-grid-post">';
            $content .= '<div class="fl-post-grid-image prod_like_wrap mesureMentWrap">';

            $content .= '<a href="javascript:void(0);" title="#" class="wplightbox"><img class="measure_img" src="'.home_url().''.$img_path.'" /></a>';

            $content .= '<div class="favButtons button-wrapper">
                <a class="button fl-button view" href="javascript:void(0);" title="#"  data-img="'.home_url().''.$img_path.'" onclick="openPopUp(this)" data-title="'.$img->image_name.'">VIEW</a>';
                $content .= '<a class="button fl-button deletemeasure" href="#" data-id="'.$img->id.'"><i class="fa fa-trash" aria-hidden="true"></i></a>'; 
                $content .= '</div> </div>';
                $content .= '<h3><span>'.$img->image_name.'</span></h3>';

                $content .= "</div></div>";

        }

        $content .= "</div></div>";
        

        return $content;
    }
   
  
}    
add_shortcode('measurementtool_images', 'measurement_tool_images');


add_action( 'wp_ajax_nopriv_delete_measureimg', 'delete_measurement_images' );
add_action( 'wp_ajax_delete_measureimg', 'delete_measurement_images' );

function delete_measurement_images() {  
    global $wpdb;

    $mid = $_POST['mimg_id'];

    if ( is_user_logged_in() ) {
        $current_user = wp_get_current_user();
        write_log( 'Personal Message For '. $current_user->ID ) ;

        $wpdb->get_results('DELETE FROM wp_measure_images WHERE userid = '.$current_user->ID.' and id = '.$mid.'');
        

        $measureimages = $wpdb->get_results("SELECT * FROM wp_measure_images WHERE userid = $current_user->ID ");

        write_log($measureimages);

        
        $content = '<div id="mesureMentprintMe" class="product-plp-grid product-grid swatch "><div class="row product-row" id="aniimated-thumbnials">';

        $content .= '<h3 style="text-align: center; font-size: 33px;"><span>SAVED MEASUREMENTS</span></h3>';

        foreach($measureimages as $img){

           

            $img_path = str_replace('/var/www/html', '', $img->image_path);

            $content .= '<div class="col-lg-4 col-md-4 col-sm-6 "><div class="fl-post-grid-post">';
            $content .= '<div class="fl-post-grid-image prod_like_wrap mesureMentWrap">';

            $content .= '<a href="'.home_url().''.$img_path.'" title="#" class="wplightbox"><img class="measure_img" src="'.home_url().''.$img_path.'" /></a>';

            $content .= '<div class="favButtons button-wrapper">
                <a class="button fl-button" href="javascript:void(0)" title="#" onclick="openPopUp(this)"   data-img="'.home_url().''.$img_path.'" data-title="'.$img->image_name.'" onclik="openPopUp(e)">VIEW</a>';
                $content .= '<a class="button fl-button deletemeasure" href="#" data-id="'.$img->id.'"><i class="fa fa-trash" aria-hidden="true"></i></a>'; 
                $content .= '</div> </div>';
                $content .= '<h3><span>'.$img->image_name.'</span></h3>';

                $content .= "</div></div>";

        }

        $content .= "</div></div>";

        echo $content;
    }

  wp_die();

}

add_action( 'wp_ajax_nopriv_add_favroiute', 'add_favroiute' );
add_action( 'wp_ajax_add_favroiute', 'add_favroiute' );

function add_favroiute() {

    global $wpdb;

    if ( is_user_logged_in() ) {
        $current_user = wp_get_current_user();
        write_log( 'Personal Message For '. $current_user->ID ) ;
     
        $data=array(
            'user_id' => $current_user->ID, 
            'product_id' => $_POST['post_id']           
        );    
    
         $wpdb->insert( 'wp_favorite_posts', $data);        

         return true;

         wp_die();

    }else{

        return false;

        wp_die();
    }
}

add_action( 'wp_ajax_nopriv_remove_favroiute', 'remove_favroiute' );
add_action( 'wp_ajax_remove_favroiute', 'remove_favroiute' );

function remove_favroiute() {    

    global $wpdb;

    if ( is_user_logged_in() ) {
        $current_user = wp_get_current_user();
        write_log( 'Personal Message For '. $current_user->ID ) ;
     
        $data=array(
            'user_id' => $_POST['user_id'], 
            'product_id' => $_POST['post_id']           
        );    
    
        $wpdb->get_results('DELETE FROM wp_favorite_posts WHERE user_id = '.$_POST['user_id'].' and product_id = '.$_POST['post_id'].'');

         return true;

         wp_die();

    }else{

        return false;

        wp_die();
    }

}

// Favorite product shortcodes 
function greatcustom_favorite_posts_function(){

    global $wpdb;
    $content = "";
    $fav_sql = 'SELECT product_id FROM wp_favorite_posts WHERE user_id = '.get_current_user_id().'';
            
    $check_fav = $wpdb->get_results($fav_sql);    

    write_log($check_fav);

    $fav_products = array();

    foreach($check_fav as $fav){

        $fav_products[] = $fav->product_id;
    }

    write_log('dsfsd'.$fav_products);
    
    $favorite_post_ids = array_reverse($fav_products);
    $page = intval(get_query_var('paged'));

    $post_array = array('carpeting','hardwood_catalog','laminate_catalog','luxury_vinyl_tile','tile_catalog', 'instock_laminate');

        $brandmapping = array(
            "Carpet"=>"carpeting",
            "Hardwood"=>"hardwood_catalog",
            "Laminate"=>"laminate_catalog",
            "Instock Laminate"=>"instock_laminate",
            "Luxury Vinyl Tile"=>"luxury_vinyl_tile",
            "Ceramic Tile"=>"tile_catalog",
            "Waterproof"=>"solid_wpc_waterproof"
        );
    

   

    $content .= '<div id="ajaxreplace"><div class="favProWarpper printWrap">
                    <div class="shareAndPrint hideShare">
                        <a href="#"  id="share-button-3" class="need-share-button-default" data-share-position="middleBottom" data-share-networks="Mailto,Twitter,Pinterest,Facebook,GooglePlus,Linkedin"></a>
                        <a href="#" class="printDOc" ><i class="fa fa-print" aria-hidden="true"></i></a>
                    </div>
                <div class="row statiContent">
                    <div class="col-lg-12">
                        <h2>MY FAVORITE PRODUCTS </h2>
                        <p>Our product catalog is home to thousands of products. 
                        Collect your favorites and save them here to help you select the right product for your home.</p>
                    </div>
                </div>';
    
    if(!empty($check_fav)) {

        
        foreach($post_array as $posttype){

            $qry = array('post__in' => $favorite_post_ids, 'posts_per_page'=> $post_per_page, 'orderby' => 'post__in', 'paged' => $page);
            // custom post type support can easily be added with a line of code like below.
           //  $qry['post_type'] = array('carpeting','hardwood_catalog','laminate_catalog','luxury_vinyl_tile','tile_catalog');
           $qry['post_type'] = array($posttype);
           
           query_posts($qry);
    
           $post_type_name = array_search($posttype,$brandmapping);
            
           $content .= '<div id="printMe" class="product-plp-grid product-grid swatch "><div class="row product-row">';
            if ( have_posts() ) :
                $content .= '<h3>'.$post_type_name.'</h3>';
            endif;
            while ( have_posts() ) : the_post();

            $note_sql = 'SELECT note FROM wp_favorite_posts WHERE user_id = '.get_current_user_id().' and product_id='.get_the_ID().'';
            
            $check_note = $wpdb->get_results($note_sql, ARRAY_A);  
            // write_log('check_note');
            // write_log($check_note[0]['note']);
               
            $content .= '<div class="col-lg-4 col-md-4 col-sm-6 "><div class="fl-post-grid-post">';
            $content .= '<div class="fl-post-grid-image prod_like_wrap">';
    
                $image = swatch_image_product_thumbnail(get_the_ID(),'222','222');
    
                $sku = get_post_meta(get_the_ID(),'sku',true);
                $manufacturer = get_post_meta(get_the_ID(),'manufacturer',true);
    
                $content .= '<a href="'.get_permalink().'" title="'.get_the_title() .'"><img crossorigin="Anonymous"  class="list-pro-image" src="'. $image.'" alt="'.get_the_title().'" /></a>';
    
                $content .= '<div class="favButtons button-wrapper">
                    <a class="button fl-button" href="'.get_permalink().'" title="'.get_the_title() .'">VIEW PRODUCT</a>';
                   // $content .=   roomvo_script_integration($manufacturer,$sku,get_the_ID());
                   if($check_note[0]['note'] == '' || $check_note[0]['note'] == null){
                    $content .= '<a class="button fl-button add_note_fav" id="add_note_fun" href="javascript:void(0)" data-productid="'.get_the_ID().'">ADD A NOTE</a>'; 
                   }
                    $content .= '<div class="buttonHolder"><a id="rem_fav" data-id="'.get_the_ID().'" class="remove-parent" data-user_id="'.get_current_user_id().'" href="javascript:void(0)"><i class="fa fa-trash" aria-hidden="true"></i></a>';

                    if($check_note[0]['note'] != '' || $check_note[0]['note'] != null){
                        $content .= '<a class="view_note_fav" data-note="'.$check_note[0]['note'].'" id="view_note_fun" href="javascript:void(0)" data-productid="'.get_the_ID().'"><i class="fa fa-sticky-note" aria-hidden="true"></i></a>'; 
                        // $content .= '<div id="'.get_the_ID().'" style="display:none">'.$check_note[0]['note'].'</div>';
                       }
                    $content .= '</div></div> </div>';
                    $content .= ' <div class="fl-post-grid-text product-grid btn-grey">';
                    $content .= '<h4><span>'.get_the_title().'</span></h4>';
                    $content .= '</div>';
    
                //wpfp_remove_favorite_link(get_the_ID());
                $content .= "</div></div>";
            endwhile;
            $content .= "</div></div>";
        
    
        }
        echo '</div></div>';

    return $content;

 }


}
add_shortcode('greatcustom_favorite_posts', 'greatcustom_favorite_posts_function');


add_action( 'wp_ajax_nopriv_remove_favroiute_list', 'remove_greatcustom_favorite_posts_function' );
add_action( 'wp_ajax_remove_favroiute_list', 'remove_greatcustom_favorite_posts_function' );
function remove_greatcustom_favorite_posts_function(){

    global $wpdb;

    $content = "";

    $wpdb->get_results('DELETE FROM wp_favorite_posts WHERE product_id = '.$_POST['post_id'].' and user_id = '.get_current_user_id().'');    

    $fav_sql = 'SELECT product_id FROM wp_favorite_posts WHERE user_id = '.get_current_user_id().'';
            
    $check_fav = $wpdb->get_results($fav_sql);    

    write_log($check_fav);

    $fav_products = array();

    foreach($check_fav as $fav){

        $fav_products[] = $fav->product_id;
    }

    write_log('dsfsd'.$fav_products);
    
    $favorite_post_ids = array_reverse($fav_products);
    $page = intval(get_query_var('paged'));

    $post_array = array('carpeting','hardwood_catalog','laminate_catalog','luxury_vinyl_tile','tile_catalog');

        $brandmapping = array(
            "Carpet"=>"carpeting",
            "Hardwood"=>"hardwood_catalog",
            "Laminate"=>"laminate_catalog",
            "Luxury Vinyl Tile"=>"luxury_vinyl_tile",
            "Ceramic Tile"=>"tile_catalog",
            "Waterproof"=>"solid_wpc_waterproof"
        );
    

   

    $content .= '<div class="favProWarpper printWrap">
                    <div class="shareAndPrint hideShare">
                        <a href="#"  id="share-button-3" class="need-share-button-default" data-share-position="middleBottom" data-share-networks="Mailto,Twitter,Pinterest,Facebook,GooglePlus,Linkedin"></a>
                        <a href="#" class="printDOc" ><i class="fa fa-print" aria-hidden="true"></i></a>
                    </div>
                <div class="row statiContent">
                    <div class="col-lg-12">
                        <h2>MY FAVORITE PRODUCTS </h2>
                        <p>Our product catalog is home to thousands of products. 
                        Collect your favorites and save them here to help you select the right product for your home.</p>
                    </div>
                </div>';
    
    if(!empty($check_fav)) {

        
        foreach($post_array as $posttype){

            $qry = array('post__in' => $favorite_post_ids, 'posts_per_page'=> $post_per_page, 'orderby' => 'post__in', 'paged' => $page);
            // custom post type support can easily be added with a line of code like below.
           //  $qry['post_type'] = array('carpeting','hardwood_catalog','laminate_catalog','luxury_vinyl_tile','tile_catalog');
           $qry['post_type'] = array($posttype);
           
           query_posts($qry);
    
           $post_type_name = array_search($posttype,$brandmapping);
            
           $content .= '<div id="printMe" class="product-plp-grid product-grid swatch "><div class="row product-row">';
            if ( have_posts() ) :
                $content .= '<h3>'.$post_type_name.'</h3>';
            endif;
            while ( have_posts() ) : the_post();

            $note_sql = 'SELECT note FROM wp_favorite_posts WHERE user_id = '.get_current_user_id().' and product_id='.get_the_ID().'';
            
            $check_note = $wpdb->get_results($note_sql, ARRAY_A);  
            write_log('check_note');
            write_log($check_note[0]['note']);
               
            $content .= '<div class="col-lg-4 col-md-4 col-sm-6 "><div class="fl-post-grid-post">';
            $content .= '<div class="fl-post-grid-image prod_like_wrap">';
    
                $image = swatch_image_product_thumbnail(get_the_ID(),'222','222');
    
                $sku = get_post_meta(get_the_ID(),'sku',true);
                $manufacturer = get_post_meta(get_the_ID(),'manufacturer',true);
    
                $content .= '<a href="'.get_permalink().'" title="'.get_the_title() .'"><img class="list-pro-image" crossorigin="Anonymous"  src="'. $image.'" alt="'.get_the_title().'" /></a>';
    
                $content .= '<div class="favButtons button-wrapper">
                    <a class="button fl-button" href="'.get_permalink().'" title="'.get_the_title() .'">VIEW PRODUCT</a>';
                   // $content .=   roomvo_script_integration($manufacturer,$sku,get_the_ID());
                   if($check_note[0]['note'] == '' || $check_note[0]['note'] == null){
                    $content .= '<a class="button fl-button add_note_fav" id="add_note_fun" href="javascript:void(0)" data-productid="'.get_the_ID().'">ADD A NOTE</a>'; 
                   }
                    $content .= '<div class="buttonHolder"><a id="rem_fav" data-id="'.get_the_ID().'" class="remove-parent" data-user_id="'.get_current_user_id().'" href="javascript:void(0)"><i class="fa fa-trash" aria-hidden="true"></i></a>';

                    if($check_note[0]['note'] != '' || $check_note[0]['note'] != null){
                        $content .= '<a class="view_note_fav" data-note="'.$check_note[0]['note'].'" id="view_note_fun" href="javascript:void(0)" data-productid="'.get_the_ID().'"><i class="fa fa-sticky-note" aria-hidden="true"></i></a>'; 
                        $content .= '<div id="'.get_the_ID().'" style="display:none">'.$check_note[0]['note'].'</div>';
                       }
                    $content .= '</div></div> </div>';
                    $content .= ' <div class="fl-post-grid-text product-grid btn-grey">';
                    $content .= '<h4><span>'.get_the_title().'</span></h4>';
                    $content .= '</div>';
    
                //wpfp_remove_favorite_link(get_the_ID());
                $content .= "</div></div>";
            endwhile;
            $content .= "</div></div>";
        
    
        }

        $content .='</div></div>';
        

    echo $content;

    wp_die();

 }


}

add_action('wp_ajax_add_note_form', 'add_note_form');
add_action('wp_ajax_nopriv_add_note_form', 'add_note_form');

function add_note_form()
{
    global $wpdb;

    $fav_sql = 'UPDATE wp_favorite_posts SET note = "'.$_POST['note'].'" WHERE user_id = '.get_current_user_id().' and product_id = '.$_POST['addnote_productid'].'';
            
    $check_fav = $wpdb->get_results($fav_sql); 

    echo $fav_sql;
}
//Yoast SEO Breadcrumb link - Changes for PDP pages
add_filter( 'wpseo_breadcrumb_links', 'wpse_100012_override_yoast_breadcrumb_trail1' );

function wpse_100012_override_yoast_breadcrumb_trail1( $links ) {
    global $post;

    if ( is_singular( 'hardwood_catalog' )  ) {
        // $breadcrumb[] = array(
        //     'url' => get_site_url().'/hardwood/',
        //     'text' => 'Flooring',
        // );
        $breadcrumb[] = array(
            'url' => get_site_url().'/products/',
            'text' => 'Products',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/products/hardwood/',
            'text' => 'Hardwood',
        );
        // $breadcrumb[] = array(
        //     'url' => get_site_url().'/flooring/hardwood/products/',
        //     'text' => 'Products',
        // );
        array_splice( $links, 1, -1, $breadcrumb );

    }elseif (is_singular( 'carpeting' )) {
        // $breadcrumb[] = array(
        //     'url' => get_site_url().'/flooring/',
        //     'text' => 'Flooring',
        // );
        $breadcrumb[] = array(
            'url' => get_site_url().'/products/',
            'text' => 'Products',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/products/carpet/',
            'text' => 'Carpet',
        );
       
        array_splice( $links, 1, -1, $breadcrumb );

    }elseif (is_singular( 'luxury_vinyl_tile' )) {

        // $breadcrumb[] = array(
        //     'url' => get_site_url().'/flooring/',
        //     'text' => 'Flooring',
        // );
        $breadcrumb[] = array(
            'url' => get_site_url().'/products/',
            'text' => 'Products',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/products/luxury-vinyl/',
            'text' => 'Luxury Vinyl',
        );
       
        array_splice( $links, 1, -1, $breadcrumb );
        
    }elseif (is_singular( 'laminate_catalog' )) {

        // $breadcrumb[] = array(
        //     'url' => get_site_url().'/flooring/',
        //     'text' => 'Flooring',
        // );
        $breadcrumb[] = array(
            'url' => get_site_url().'/products/',
            'text' => 'Products',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/products/laminate/',
            'text' => 'Laminate',
        );
        
        array_splice( $links, 1, -1, $breadcrumb );
        
    }elseif (is_singular( 'tile_catalog' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/products/',
            'text' => 'Products',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/products/tile-and-stone/',
            'text' => 'Tile',
        );
        
        array_splice( $links, 1, -1, $breadcrumb );
        
    }

    return $links;
}
function fwp_disable_auto_refresh() {
    ?>
    <script>
    (function($) {
        $(function() {
            if ('undefined' !== typeof FWP) {
                FWP.auto_refresh = true;
            }
        });
    })(jQuery);
    </script>
    <?php
    }
    add_action( 'wp_head', 'fwp_disable_auto_refresh', 100 );

    //Roomvo script
add_action('astra_footer_after', 'custom_roomvo_custom_footer_js');
function custom_roomvo_custom_footer_js() {

    $website_json_data = json_decode(get_option('website_json'));

    foreach($website_json_data->sites as $site_cloud){
            
        if($site_cloud->instance == 'prod'){
    
            if( $site_cloud->roomvo == 'true'){
    
  echo "<script src='https://www.roomvo.com/static/scripts/b2b/mobilemarketing.js' async></script>";

            }
        }
    }
}

add_filter( 'gform_admin_pre_render_32', 'populate_product_location_form' );
function populate_product_location_form( $form ) {

    foreach ( $form['fields'] as &$field ) {

        // Only populate field ID 12
        if( $field['id'] == 15 ) {           	

            $args = array(
                'post_type'      => 'store-locations',
                'posts_per_page' => -1,
                'post_status'    => 'publish'
            );										
          
             $locations =  get_posts( $args );

             $choices = array(); 

             foreach($locations as $location) {
                

                  $title = get_the_title($location->ID);
                  
                       $choices[] = array( 'text' => $title, 'value' => $title );

              }
              wp_reset_postdata();

            $field->placeholder = '-- Choose Location --';
            
             $field->choices = $choices;

        }
   }
return $form;

}
add_action('after_setup_theme', 'remove_admin_bar');
function remove_admin_bar() {
if (!current_user_can('administrator') && !is_admin()) {
  show_admin_bar(false);
}
}


//add method to register event to WordPress init

add_action( 'init', 'register_daily_mysql_bin_log_event');
 
function register_daily_mysql_bin_log_event() {
    // make sure this event is not scheduled
    if( !wp_next_scheduled( 'mysql_bin_log_job' ) ) {
        // schedule an event
        wp_schedule_event( time(), 'daily', 'mysql_bin_log_job' );
    }
}

add_action( 'mysql_bin_log_job', 'mysql_bin_log_job_function' );
 

function mysql_bin_log_job_function() {
   
    global $wpdb;
    $yesterday = date('Y-m-d',strtotime("-1 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'" ;						
	$delete_endpoint = $wpdb->get_results($sql_delete);
    write_log($sql_delete);	
}


//Accessibility employee shortcode
function accessibility_shortcodes($arg) {  

    $website_json =  json_decode(get_option('website_json'));
     

     foreach($website_json->contacts as $contact){

        if($contact->accessibility == '1'){ 

            if (in_array("phone", $arg)) {

                $content = '<a href="tel:'.$contact->phone.'">'.$contact->phone.'</a>';                
               
            }
            if (in_array("email", $arg)) {

                $content = '<a href="mailto:'.$contact->email.'">'.$contact->email.'</a>';              
               
            }
        }
    }

    return  $content;
    
}
add_shortcode('accessibility', 'accessibility_shortcodes');

//Custom Banner home page banner hook
function astra_custom_alert_banner() {  

    $website_json =  json_decode(get_option('website_json'));
     

     foreach($website_json->sites as $site_chat){

        if($site_chat->instance == 'prod'){ 

            if ( ($site_chat->banner != null || $site_chat->banner != '') &&  is_front_page() ) {

                $content = '<div class="custombanneralert" style="background-color:'.$website_json->color1.';">'.$site_chat->banner.'</div>';
                
                echo $content;
            }
        }
    }
    
}
add_action( 'astra_header_after', 'astra_custom_alert_banner' );





function storelocation_Fwphone_tel($arg)
{
    
    $website = json_decode(get_option('website_json'));
   
    for ($i=0;$i < count($website->locations);$i++) {
        $location_name = isset($website->locations[$i]->name) ? $website->locations[$i]->name:"";

        $location_ids = isset($website->locations[$i]->id) ? $website->locations[$i]->id:"";
       
        if($website->locations[$i]->type == 'store' &&  $website->locations[$i]->name !=''){

            
            if (in_array(trim($location_name), $arg) || in_array(trim($location_ids), $arg)){

                if($website->locations[$i]->name == 'Main' || $website->locations[$i]->name=="MAIN"){

                    $location_name = get_bloginfo( 'name' );
                    
                }else{
                  
                    $location_name =  isset($website->locations[$i]->name) ? $website->locations[$i]->name." ":"";

                }
      

                $location_phone  = "";
                if (isset($website->locations[$i]->phone)) {
                    $location_tel = preg_replace('/[^0-9]/', '', $website->locations[$i]->phone);
                    $location_phone  = formatPhoneNumber($website->locations[$i]->phone);
                }
                if (isset($website->locations[$i]->forwardingPhone) && $website->locations[$i]->forwardingPhone != "") {
                    $forwarding_tel = preg_replace('/[^0-9]/', '', $website->locations[$i]->forwardingPhone);
                    $forwarding_phone  = formatPhoneNumber($website->locations[$i]->forwardingPhone);
                    
                }else if (isset($website->locations[$i]->phone) && $website->locations[$i]->forwardingPhone = "") {

                    $location_tel = preg_replace('/[^0-9]/', '', $website->locations[$i]->phone);
                    $location_phone  = formatPhoneNumber($website->locations[$i]->phone);
                }
                else{
        
                    $forwarding_tel ="#";
                    $forwarding_phone  = formatPhoneNumber(8888888888);
                }

                if (in_array("forwardingphoneTel", $arg)) {
                    $locations = $forwarding_tel;    
                }
                if (in_array("forwardingphoneTxt", $arg)) {

                    $locations = $forwarding_phone;     
                }
                
            } 

        }        
    }

    return $locations;
}
add_shortcode('storelocation_Fwphone_tel', 'storelocation_Fwphone_tel');
