<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Astra
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>
<?php astra_content_bottom(); ?>
	</div> <!-- ast-container -->
	</div><!-- #content -->
<?php 
	astra_content_after();
		
	astra_footer_before();
		
	astra_footer();
		
	astra_footer_after(); 
?>
	</div><!-- #page -->
<?php 
	astra_body_bottom();    
	wp_footer(); 
?>

<script src="https://calculator.measuresquare.com/scripts/jquery-m2FlooringCalculator.js"></script> 

<script>
           $(function () {
               $('.calculateBtn').m2Calculator({
                   measureSystem: "Imperial",            
                   thirdPartyName: "Total Flooring Source", 
                   thirdPartyEmail: "tomu@greatlakescarpet.com",  // if showDiagram = false, will send estimate data to this email when user click Email Estimate button
                   showCutSheet: false, // if false, will not include cutsheet section in return image
                   showDiagram: true,  // if false, will close the popup directly 
                 /*  product: {
                       type: "Carpet",
                       name: "Carpet 1",
                       width: "6'0\"",
                       length: "150'0\"",
                       horiRepeat: "3'0\"",
                       vertRepeat: "3'0\"",
                       horiDrop: "",
                       vertDrop: ""
                   },
                   */
                   cancel: function () {
                       //when user closes the popup without calculation.
                   },
                   callback: function (data) {
                       //json format, include user input, usage and base64image

                       var json = JSON.parse(JSON.stringify(data));

                       var res = JSON.stringify(data);
                     //  $("#callback").html(JSON.stringify(data));
                       var json1 = JSON.parse(JSON.stringify(json.input));                      
                       var imageName = '';        
                       if (!data.input.rooms.length){
                        imageName = data.input.stairs[0].name;
                       }else{
                        imageName = data.input.rooms[0].name;
                       }
                       $.ajax({
                           type: "POST",
                           url: "/wp-admin/admin-ajax.php",
                           data: 'action=base64_to_jpeg_convert&imagedata=' + data.img + '&imagename='+ imageName,
                           success: function(data) {
                               jQuery("#mesureMentprintMe").html(data);
                           }

                       });
                   }
               });
           });


   $(function() {
   $('.calculateBtninstock').m2Calculator({
       measureSystem: "Imperial",
       thirdPartyName: "Total Flooring Source",
       thirdPartyEmail: "devteam@mobile-marketing.agency", // if showDiagram = false, will send estimate data to this email when user click Email Estimate button
       showCutSheet: false, // if false, will not include cutsheet section in return image
       showDiagram: true, // if false, will close the popup directly 

       cancel: function() {
           //when user closes the popup without calculation.
       },
       callback: function(data) {
           //json format, include user input, usage and base64image
           $("#callback").html(JSON.stringify(data));
           console.log(data.input)
           $("#input_32_3").val(data.usage);
           $("#input_35_3").val(data.usage);
           $("#input_34_3").val(data.usage);
           $("#input_37_3").val(data.usage);

           // $("#image").attr("src", data.img);  //base64Image 
           //window.location.href = "https://www.floorstores.com/thank-you-contact-us/";
       }
   });
});        
</script>
	</body>
</html>
